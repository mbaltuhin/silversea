import datetime
import os

import requests
from multiprocessing.dummy import Pool as ThreadPool
import xlsxwriter


def write_file_to_excell(array):
    userhome = os.path.expanduser('~')
    now = datetime.datetime.now()
    path_to_file = userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(
        now.day) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '-SilverSea.xlsx'
    if not os.path.exists(userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(
            now.month) + '-' + str(now.day)):
        os.makedirs(
            userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(now.day))
    workbook = xlsxwriter.Workbook(path_to_file)

    worksheet = workbook.add_worksheet()
    worksheet.set_column("A:A", 15)
    worksheet.set_column("B:B", 25)
    worksheet.set_column("C:C", 10)
    worksheet.set_column("D:D", 25)
    worksheet.set_column("E:E", 20)
    worksheet.set_column("F:F", 30)
    worksheet.set_column("G:G", 20)
    worksheet.set_column("H:H", 50)
    worksheet.set_column("I:I", 20)
    worksheet.set_column("J:J", 20)
    worksheet.set_column("K:K", 20)
    worksheet.set_column("L:L", 20)
    worksheet.set_column("M:M", 25)
    worksheet.set_column("N:N", 20)
    worksheet.set_column("O:O", 20)
    worksheet.set_column("P:P", 21)
    worksheet.write('A1', 'DestinationCode')
    worksheet.write('B1', 'DestinationName')
    worksheet.write('C1', 'VesselID')
    worksheet.write('D1', 'VesselName')
    worksheet.write('E1', 'CruiseID')
    worksheet.write('F1', 'CruiseLineName')
    worksheet.write('G1', 'ItineraryID')
    worksheet.write('H1', 'BrochureName')
    worksheet.write('I1', 'NumberOfNights')
    worksheet.write('J1', 'SailDate')
    worksheet.write('K1', 'ReturnDate')
    worksheet.write('L1', 'InteriorBucketPrice')
    worksheet.write('M1', 'OceanViewBucketPrice')
    worksheet.write('N1', 'BalconyBucketPrice')
    worksheet.write('O1', 'SuiteBucketPrice')
    worksheet.write('P1', 'PortList')
    col = 0
    row = 1
    for result in array:
        for item in result:
            if col == {11, 12, 13, 14}:
                try:
                    worksheet.write_number(row, col, item)
                except ValueError:
                    worksheet.write_string(row, col, str(item))
            elif col == {9, 10}:
                worksheet.write_datetime(row, col, item)
            else:
                worksheet.write_string(row, col, str(item))
            col += 1
        row += 1
        col = 0
    workbook.close()


def parse(result):
    cruise_id = result["cruiseCode"]
    destination_id = result["destinationId"]
    itinerary_id = ""
    destination_name = result["destinationName"]
    duration = result["days"]
    available = result["available"]
    ports = result["portNames"]
    ship_name = result["content"]["shipName"]
    ship_id = result["content"]["shipId"]
    departure_date_formatted = datetime.datetime.strptime(result["departurePort"]['itineraryDate'],
                                                          '%Y-%m-%d').date()
    arrival_date_formatted = datetime.datetime.strptime(result["arrivalPort"]['itineraryDate'], '%Y-%m-%d').date()
    price_details = session.get("https://api.digital.silversea.com/graphql/cruisePriceAndOffers/" + cruise_id).json()
    interior_price = "N/A"
    oceanview_price = 0
    balcony_price = 0
    suite_price = 0
    if available:
        if ship_name in "Silver Cloud":
            for room in price_details["suitePrices"]:
                if room["suiteCategory"] in "VI":
                    try:
                        oceanview_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["VI", "DX"]:
                    try:
                        if balcony_price == 0:
                            balcony_price = room["priceFrom"]["amount"]
                        else:
                            if balcony_price > room["priceFrom"]["amount"]:
                                balcony_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["ME", "SL", "R1", "G1", "R2", "O1", "G2", "O2"]:
                    try:
                        if suite_price == 0:
                            suite_price = room["priceFrom"]["amount"]
                        else:
                            if suite_price > room["priceFrom"]["amount"]:
                                suite_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
        elif ship_name in "Silver Explorer":
            for room in price_details["suitePrices"]:
                if room["suiteCategory"] in ["AC", "EC", "VS", "VI"]:
                    try:
                        if oceanview_price == 0:
                            oceanview_price = room["priceFrom"]["amount"]
                        else:
                            if oceanview_price > room["priceFrom"]["amount"]:
                                oceanview_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["VR"]:
                    try:
                        if balcony_price == 0:
                            balcony_price = room["priceFrom"]["amount"]
                        else:
                            if balcony_price > room["priceFrom"]["amount"]:
                                balcony_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["ME", "SL", "G1", "O1"]:
                    try:
                        if suite_price == 0:
                            suite_price = room["priceFrom"]["amount"]
                        else:
                            if suite_price > room["priceFrom"]["amount"]:
                                suite_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
        elif ship_name in "Silver Shadow":
            for room in price_details["suitePrices"]:
                if room["suiteCategory"] in ["VI"]:
                    try:
                        if oceanview_price == 0:
                            oceanview_price = room["priceFrom"]["amount"]
                        else:
                            if oceanview_price > room["priceFrom"]["amount"]:
                                oceanview_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["CV", "SV", "DX"]:
                    try:
                        if balcony_price == 0:
                            balcony_price = room["priceFrom"]["amount"]
                        else:
                            if balcony_price > room["priceFrom"]["amount"]:
                                balcony_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["ME", "SL", "R1", "G1", "O1"]:
                    try:
                        if suite_price == 0:
                            suite_price = room["priceFrom"]["amount"]
                        else:
                            if suite_price > room["priceFrom"]["amount"]:
                                suite_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
        elif ship_name in "Silver Origin":
            for room in price_details["suitePrices"]:
                if room["suiteCategory"] in ["XX"]:
                    try:
                        if oceanview_price == 0:
                            oceanview_price = room["priceFrom"]["amount"]
                        else:
                            if oceanview_price > room["priceFrom"]["amount"]:
                                oceanview_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["CV", "SV", "DX"]:
                    try:
                        if balcony_price == 0:
                            balcony_price = room["priceFrom"]["amount"]
                        else:
                            if balcony_price > room["priceFrom"]["amount"]:
                                balcony_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["ME", "SL", "R1", "G1", "O1"]:
                    try:
                        if suite_price == 0:
                            suite_price = room["priceFrom"]["amount"]
                        else:
                            if suite_price > room["priceFrom"]["amount"]:
                                suite_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
        elif ship_name in "Silver Whisper":
            for room in price_details["suitePrices"]:
                if room["suiteCategory"] in ["VI"]:
                    try:
                        if oceanview_price == 0:
                            oceanview_price = room["priceFrom"]["amount"]
                        else:
                            if oceanview_price > room["priceFrom"]["amount"]:
                                oceanview_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["CV", "SV", "DX"]:
                    try:
                        if balcony_price == 0:
                            balcony_price = room["priceFrom"]["amount"]
                        else:
                            if balcony_price > room["priceFrom"]["amount"]:
                                balcony_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["ME", "SL", "R1", "G1", "O1"]:
                    try:
                        if suite_price == 0:
                            suite_price = room["priceFrom"]["amount"]
                        else:
                            if suite_price > room["priceFrom"]["amount"]:
                                suite_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
        elif ship_name in "Silver Wind":
            for room in price_details["suitePrices"]:
                if room["suiteCategory"] in ["VI"]:
                    try:
                        if oceanview_price == 0:
                            oceanview_price = room["priceFrom"]["amount"]
                        else:
                            if oceanview_price > room["priceFrom"]["amount"]:
                                oceanview_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["CV", "DX"]:
                    try:
                        if balcony_price == 0:
                            balcony_price = room["priceFrom"]["amount"]
                        else:
                            if balcony_price > room["priceFrom"]["amount"]:
                                balcony_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["ME", "SL", "R1", "G1", "O1"]:
                    try:
                        if suite_price == 0:
                            suite_price = room["priceFrom"]["amount"]
                        else:
                            if suite_price > room["priceFrom"]["amount"]:
                                suite_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
        elif ship_name in ["Silver Dawn", "Silver Moon", "Silver Muse", "Silver Spirit"]:
            for room in price_details["suitePrices"]:
                if room["suiteCategory"] in ["VI", "PA"]:
                    try:
                        if oceanview_price == 0:
                            oceanview_price = room["priceFrom"]["amount"]
                        else:
                            if oceanview_price > room["priceFrom"]["amount"]:
                                oceanview_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["CV", "SV", "DX"]:
                    try:
                        if balcony_price == 0:
                            balcony_price = room["priceFrom"]["amount"]
                        else:
                            if balcony_price > room["priceFrom"]["amount"]:
                                balcony_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
                elif room["suiteCategory"] in ["SL", "R1", "G1", "O1"]:
                    try:
                        if suite_price == 0:
                            suite_price = room["priceFrom"]["amount"]
                        else:
                            if suite_price > room["priceFrom"]["amount"]:
                                suite_price = room["priceFrom"]["amount"]
                    except KeyError:
                        pass
        if oceanview_price == 0:
            oceanview_price = "N/A"
        if balcony_price == 0:
            balcony_price = "N/A"
        if suite_price == 0:
            suite_price = "N/A"
        parsed_results.append(
            [destination_id, destination_name, ship_id, ship_name, cruise_id, "SilverSea Cruises", itinerary_id, "",
             duration, departure_date_formatted, arrival_date_formatted, interior_price, oceanview_price, balcony_price,
             suite_price, ports])
    else:
        interior_price = "N/A"
        oceanview_price = "N/A"
        balcony_price = "N/A"
        suite_price = "N/A"

        parsed_results.append(
            [destination_id, destination_name, ship_id, ship_name, cruise_id, "SilverSea Cruises", itinerary_id, "",
             duration, departure_date_formatted, arrival_date_formatted, interior_price, oceanview_price, balcony_price,
             suite_price, ports])


pool = ThreadPool(5)

headers = {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en,bg;q=0.9",
    "Connection": "keep-alive",
    "x-algolia-application-id": "OGG7AV1JSP",
    "x-algolia-api-key": "991277d62f48eae2c54ad02824d25457",
    "Host": "ogg7av1jsp-dsn.algolia.net",
    "Origin": "https://www.silversea.com",
    "Referer": "https://www.silversea.com/",
    "sec-ch-ua": '"Google Chrome";v="87", " Not;A Brand";v="99", "Chromium";v="87"',
    "accept": "application/json",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "cross-site",
    "content-type": "application/x-www-form-urlencoded",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"

}
data = {"requests": [{"indexName": "prod_cruises_en",
                      "params": "highlightPreTag=%3Cais-highlight-0000000000%3E&highlightPostTag=%3C%2Fais-highlight-0000000000%3E&filters=(departureTimestamp%20%3E%3D%201609106400)%20AND%20(visible%3Atrue)&hitsPerPage=1000&distinct=true&query=&maxValuesPerFacet=1000&page=0&facets=%5B%22destinationName%22%2C%22departureYearMonth%22%2C%22dayGroup%22%2C%22content.shipName%22%2C%22cruiseType%22%2C%22available%22%2C%22hasAllInclusiveFares%22%5D&tagFilters="}]}

session = requests.session()
parsed_results = []
response = session.post(
    "https://ogg7av1jsp-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(4.8.3)%3B%20Browser%3B%20JS%20Helper%20(3.2.2)%3B%20react%20(16.13.1)%3B%20react-instantsearch%20(6.8.2)",
    headers=headers, json=data).json()["results"]
results = response[0]["hits"]
pool.map(parse, results)
pool.close()
pool.join()

write_file_to_excell(parsed_results)

input("Press ENTER key twice to continue...")
